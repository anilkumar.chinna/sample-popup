import { Component, OnInit } from '@angular/core';
import { Animal } from '../model/animal';
import { Details } from '../model/details';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  selectedAnimal: Animal;
  details: Details[] = null;
  animals: Animal[] = [{
    id: 1,
    name: 'Dog'
  }, {
    id: 2,
    name: 'Cat'
  }];
  dogDetails: Details[] = [{
    breeed: 'A',
    id: 1,
    name: 'AAAA'
  },
  {
    breeed: 'B',
    id: 2,
    name: 'BBBB'
  }, {
    breeed: 'C',
    id: 3,
    name: 'CCCC'
  }];
  catDetails: Details[] = [{
    breeed: 'X',
    id: 1,
    name: 'XXXX'
  },
  {
    breeed: 'Y',
    id: 2,
    name: 'YYYY'
  }, {
    breeed: 'Z',
    id: 3,
    name: 'ZZZZ'
  }];
  constructor() { }

  ngOnInit() {
    this.selectedAnimal = this.animals[0];
  }

  onSelection(id: number) {
    console.log('inside onSelection', id);
    if (id == 1) {
      this.details = this.dogDetails;
    } else {
      this.details = this.catDetails;
    }
  }

}
